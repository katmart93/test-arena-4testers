import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.add_new_project_page import AddNewProjectPage
from pages.arena_homepage import ArenaHomePage
from pages.arena_login import ArenaLoginPage
from pages.arena_project_added_popup import ArenaProjectAddedPopup
from pages.arena_projects_page import ArenaProjectsPage


@pytest.fixture()
def browser():
    # open browser
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://demo.testarena.pl/zaloguj')
    # log-in
    arena_login_page = ArenaLoginPage(browser)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    # click tools icon
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    # assert if the page is correct
    arena_projects_page = ArenaProjectsPage(browser)
    arena_projects_page.verify_title('Projekty')

    yield browser
    # close browser
    time.sleep(3)
    browser.quit()


def test_adding_new_project(browser):
    # click "add project" button
    arena_projects_page = ArenaProjectsPage(browser)
    arena_projects_page.click_add_project()
    # generate new project
    add_new_project_page = AddNewProjectPage(browser)
    add_new_project_page.create_new_project(10, 6, 20)
    # save new project
    add_new_project_page.save_new_project()
    # assert if new project has been added
    arena_project_added_popup = ArenaProjectAddedPopup(browser)
    arena_project_added_popup.verify_if_project_added()
    # go back to all projects
    add_new_project_page.click_projects_button()


def test_search_for_a_project(browser):
    # search for a project
    arena_projects_page = ArenaProjectsPage(browser)
    arena_projects_page.search_for_a_project('fPa2nd3Vfe')
