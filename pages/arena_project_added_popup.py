from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaProjectAddedPopup:
    def __init__(self, browser):
        self.browser = browser

    def verify_if_project_added(self):
        wait = WebDriverWait(self.browser, 10)
        elem_to_wait_for = (By.CSS_SELECTOR, '.icon_info')
        wait.until(expected_conditions.element_to_be_clickable(elem_to_wait_for))
