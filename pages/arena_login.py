from selenium.webdriver.common.by import By


class ArenaLoginPage:
    def __init__(self, browser):
        self.browser = browser

    def login(self, email_str, password_str):
        # login
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email_str)
        # password
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password_str)
        # log-in button
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()
