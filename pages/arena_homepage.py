from selenium.webdriver.common.by import By


class ArenaHomePage:
    def __init__(self, browser):
        self.browser = browser

    def click_tools_icon(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
