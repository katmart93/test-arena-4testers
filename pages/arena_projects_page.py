from selenium.webdriver.common.by import By


class ArenaProjectsPage:
    def __init__(self, browser):
        self.browser = browser

    def click_add_project(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a[href="http://demo.testarena.pl/administration/add_project"]')\
            .click()

    def verify_title(self, title_str):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title_str

    def search_for_a_project(self, project_name_str):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(project_name_str)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()
