from selenium.webdriver.common.by import By

from utils.random_text import generate_random_text


class AddNewProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def create_new_project(self, name_len, prefix_len, description_len):
        random_name = generate_random_text(name_len)
        random_prefix = generate_random_text(prefix_len)
        random_description = generate_random_text(description_len)
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_name)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_prefix)
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(random_description)

    def save_new_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()

    def click_projects_button(self):
        self.browser.find_element(By.CSS_SELECTOR, '.item2').click()
